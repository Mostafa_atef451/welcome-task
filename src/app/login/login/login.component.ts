import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../library/services/auth-service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  invalidForm: Boolean = false;
  constructor(private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: [
        '',
        [
          Validators.required,
        ]
      ],
      password: [
        '',
        [
          Validators.required,
        ]
      ],
    });
  }



  onSubmit() {
    if (this.form.valid) {
      console.log('form', this.form.value);
      this.authService.login(this.form.value).subscribe((data: any) => {
        localStorage.setItem('userToken', data.token);
        this.router.navigate(['/dashboard']);
      }, err => {
        this.invalidForm = true;
      });

    } else {
      // according to some business cases user should not see that there's internal server error
      // if it's ok we can say that there is some internal server error or connection error
      this.invalidForm = true;
    }
  }

}
