import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/library/services/user-service/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  err = 'Something went wrong'; // this must be put inside enums file
  users = [];
  somethingWentWrong: boolean = false;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUsers().subscribe((data: any) => {
      this.users.push(data.data);
    }, err => {
      this.somethingWentWrong = true;
    });
  }

}
